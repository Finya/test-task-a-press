app.views.GroundView = (function () {
    var groundView = function (ctx, ground) {
        app.views.BaseView.apply(this, [ctx, ground.Area]);
    };
    groundView.prototype = Object.create(app.views.BaseView.prototype);
    groundView.prototype.constructor = groundView;

    return groundView;
})(app.views || {});