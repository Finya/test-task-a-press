app.views.FieldView = (function () {
    var fieldView = function (ctx, field) {
        app.views.BaseView.apply(this, [ctx, field.Area]);
    };
    fieldView.prototype = Object.create(app.views.BaseView.prototype);
    fieldView.prototype.constructor = fieldView;

    return fieldView;
})(app.views || {});