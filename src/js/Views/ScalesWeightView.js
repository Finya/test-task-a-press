app.views.ScalesWeightView = (function () {
    var scalesWeightView = function (ctx, scaleWeight) {
        app.views.BaseView.apply(this, [ctx, scaleWeight.Area]);
    };
    scalesWeightView.prototype = Object.create(app.views.BaseView.prototype);
    scalesWeightView.prototype.constructor = scalesWeightView;

    return scalesWeightView;

})(app.views || {});