app.views.ScalesArrowView = (function () {
    var scalesArrowView = function (_ctx, scaleArrow) {
        var ctx = _ctx;
        var area = scaleArrow.Area;
        var image = new Image();
        image.src = area.getImageSource();
        this.model = scaleArrow;

        this.render = function () {
            ctx.save();
            var angle = 180 - this.model.getAngle();
            ctx.translate(area.getX(), area.getY());
            ctx.rotate(angle * Math.PI / 180);
            ctx.drawImage(
                image,
                0,
                0,
                area.getWidth(),
                area.getHeight()
            );
            ctx.restore();
        };

        this.imageLoaded = function () {
            return image.complete === true;
        }
    };

    return scalesArrowView;
})(app.views || {});