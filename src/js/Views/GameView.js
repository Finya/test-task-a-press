app.views.GameView = (function () {
    var gameView = function (gameModel, canvas, _controllerCallback) {
        var ctx = canvas.getContext('2d');
        var controllerCallback = _controllerCallback;
        var GameViews = [];
        initCanvasHandlers(canvas);
        initGameViews(gameModel.getField());

        this.renderObjects = function () {
            var loaded = true;
            for (var i = 0; i < GameViews.length && loaded; i++) {
                if (!GameViews[i].imageLoaded()) {
                    loaded = false;
                }
            }
            if (loaded) {
                this.renderObjects = draw;
            }
        };

        function draw() {
            for (var i = 0; i < GameViews.length; i++) {

                if (GameViews[i].imageLoaded()) {
                    GameViews[i].render();
                }
            }
        }

        function initCanvasHandlers(canvas) {
            canvas.onmousedown = function (e) {
                controllerCallback(e);
            };
            canvas.onmouseup = function (e) {
                controllerCallback(e);
            };
            canvas.onmousemove = function (e) {
                controllerCallback(e);
            };
        }

        function initGameViews(gameField) {
            GameViews.push(new app.views.FieldView(ctx, gameField));
            GameViews.push(new app.views.GroundView(ctx, gameField.getGround()));
            GameViews.push(new app.views.ScalesWeightView(ctx, gameField.getScales().getRightPart()));
            GameViews.push(new app.views.ScalesWeightView(ctx, gameField.getScales().getLeftPart()));
            GameViews.push(new app.views.ScalesBaseView(ctx, gameField.getScales().getBase()));
            GameViews.push(new app.views.ScalesArrowView(ctx, gameField.getScales().getArrow()));
            var bags = gameField.getBags();
            for (var i = 0; i < bags.length; i++) {
                GameViews.push(new app.views.BagView(ctx, bags[i]));
            }
        }
    };

    return gameView;

})(app.views || {});