app.views.BagView = (function () {
    var bagView = function (ctx, bag) {
        app.views.BaseView.apply(this, [ctx, bag.Area]);
    };
    bagView.prototype = Object.create(app.views.BaseView.prototype);
    bagView.prototype.constructor = bagView;

    return bagView;
})(app.views || {});