app.views.BaseView = (function () {
     var baseView = function(_ctx, _area) {
        var ctx = _ctx;
        var area = _area;
        var image = new Image();
        image.src = area.getImageSource();

        this.render = function () {
            ctx.drawImage(
                image,
                area.getX(),
                area.getY(),
                area.getWidth(),
                area.getHeight()
            );
        };

        this.imageLoaded = function () {
            return image.complete === true;
        }
    };

    return baseView;

})(app.views || {});