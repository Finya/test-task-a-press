app.views.ScalesBaseView = (function () {
    var scalesBaseView = function (ctx, scalesBase) {
        app.views.BaseView.apply(this, [ctx, scalesBase.Area]);
    };
    scalesBaseView.prototype = Object.create(app.views.BaseView.prototype);
    scalesBaseView.prototype.constructor = scalesBaseView;

    return scalesBaseView;
})(app.views || {});