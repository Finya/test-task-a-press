app.core.GameController = (function () {
    var gameController = function (field, data) {
        var gameModel = new app.core.Game(data.timer, data);
        var gameView = new app.views.GameView(gameModel, field, mouseHandler);
        setInterval(function () {
            gameModel.updateGameState();
            gameView.renderObjects();
        }, gameModel.getTimer());

        function mouseHandler(e) {
            if (e.type === "mouseup") {
                gameModel.getField().unSelectBag();
            } else {
                var point = new app.core.Point(e.clientX, e.clientY);
                if (e.type === "mousemove") {
                    gameModel.getField().moveBag(point);
                }
                else if (e.type === "mousedown") {
                    gameModel.getField().selectBag(point);
                }
            }
        }
    };

    return gameController;
})(app.core || {});