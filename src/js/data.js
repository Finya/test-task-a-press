var data = {
    timer:20,
    field: {
        width: 1366,
        height: 650,
        background: "img/bg.png"
    },
    bags: [
        {
            id: 1,
            x: 0,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b1.png',
            weight: 3
        },
        {
            id: 2,
            x: 50,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b2.png',
            weight: 3
        },
        {
            id: 3,
            x: 120,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b3.png',
            weight: 2
        },
        {
            id: 4,
            x: 190,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b4.png',
            weight: 4
        },
        {
            id: 5,
            x: 260,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b5.png',
            weight: 5
        },
        {
            id: 6,
            x: 610,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b6.png',
            weight: 3
        },
        {
            id: 7,
            x: 660,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b7.png',
            weight: 3
        },
        {
            id: 8,
            x: 1066,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b8.png',
            weight: 2
        },
        {
            id: 9,
            x: 1116,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b9.png',
            weight: 3
        },
        {
            id: 10,
            x: 1166,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b10.png',
            weight: 4
        },
        {
            id: 11,
            x: 1216,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b11.png',
            weight: 4
        },
        {
            id: 12,
            x: 1266,
            y: 200,
            width: 100,
            height: 100,
            source: 'img/bags/b12.png',
            weight: 4
        }
    ],
    scales: {
        base: {
            x: 358,
            y: 300,
            width: 650,
            height: 300,
            source: 'img/scale/scalebase.png'
        },
        left: {
            x: 330,
            y: 350,
            delta: 80,
            width: 275,
            height: 150,
            source: 'img/scale/scaleweight.png'
        },
        right: {
            x: 760,
            y: 350,
            delta: 80,
            width: 275,
            height: 150,
            source: 'img/scale/scaleweight.png'
        },
        arrow: {
            x: 703,
            y: 445,
            delta: 80,
            width: 40,
            height: 105,
            source: 'img/scale/arrow.png'
        }
    },
    ground: {
        x: 0,
        y: 575,
        width: 1366,
        height: 119,
        source: 'img/ground.png'
    }
};