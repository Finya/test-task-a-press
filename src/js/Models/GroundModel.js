app.core.GroundModel = (function () {
    var groundModel = function (x, y, width, height, source) {
        app.core.BaseModel.apply(this, arguments);
    };

    groundModel.prototype = Object.create(app.core.BaseModel.prototype);
    groundModel.prototype.constructor = groundModel;

    return groundModel;
})(app.core || {});