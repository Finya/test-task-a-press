app.core.GameField = (function () {
    var gameField = function (bagsJSON, groundJSON, fieldJSON, scaleJSON) {
        app.core.BaseModel.apply(this, [0, 0, fieldJSON.width, fieldJSON.height, fieldJSON.background]);

        var maxWeight = 0;
        var bags = [];
        var scales;
        var selectedBag = null;
        var ground = new app.core.GroundModel(
            groundJSON.x,
            groundJSON.y,
            groundJSON.width,
            groundJSON.height,
            groundJSON.source
        );
        for (var i = 0; i < bagsJSON.length; i++) {
            var bagData = bagsJSON[i];
            bags.push(new app.core.BagModel(
                bagData.x,
                bagData.y,
                bagData.width,
                bagData.height,
                bagData.source,
                bagData.id,
                bagData.weight
            ));
            maxWeight += bagData.weight;
        }

        scales = new app.core.ScaleModel(
            scaleJSON.base,
            scaleJSON.left,
            scaleJSON.right,
            scaleJSON.arrow,
            maxWeight
        );

        this.selectBag = function (point) {
            var bagsLength = bags.length;
            for (var i = 0; i < bagsLength; i++) {
                var bag = bags[i];
                if (bag.Area.intersectionWithPoint(point)) {
                    selectedBag = bag;
                    selectedBag.select();
                    scales.getRightPart().removeBag(bag);
                    scales.getLeftPart().removeBag(bag);
                    i = bagsLength;
                }
            }
        };

        this.unSelectBag = function () {
            if (selectedBag) {
                selectedBag.unselect();
                selectedBag = null;
            }
        };

        this.moveBag = function (point) {
            if (selectedBag != null) {
                selectedBag.Area.move(point, ground.Area.getY());
            }
        };

        this.updateState = function () {
            for (var i = 0; i < bags.length; i++) {
                var bag = bags[i];
                bag.checkFalling(ground, scales);
            }
            scales.updateScales();
        };

        this.getBags = function() {
            return bags;
        };

        this.getScales = function(){
            return scales;
        };

        this.getGround = function () {
            return ground;
        }
    };

    gameField.prototype = Object.create(app.core.BaseModel.prototype);
    gameField.prototype.constructor = gameField;

    return gameField;
})(app.core || {});

