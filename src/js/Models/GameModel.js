app.core.Game = (function () {
    function game(timer, dataJSON) {
        var updatingTimer = timer;
        var gameField = new app.core.GameField(
            dataJSON.bags,
            dataJSON.ground,
            dataJSON.field,
            dataJSON.scales
        );

        this.updateGameState = function () {
            gameField.updateState();
        };

        this.getTimer = function () {
            return updatingTimer;
        };

        this.getField = function () {
            return gameField;
        };
    }

    return game;
})
(app.core.Game || {});