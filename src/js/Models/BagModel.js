app.core.BagModel = (function () {
    var bagModel = function (x, y, width, height, source, _id, _weight) {
        app.core.BaseModel.apply(this, [x, y, width, height, source]);

        var fallSpeed = 0;
        var selected = false;
        var id = _id;
        var weight = _weight;

        this.checkFalling = function (ground, scales) {
            if (!selected) {
                if (this.Area.intersectionWithArea(scales.getLeftPart().getCaughtArea())) {
                    scales.getLeftPart().addBag(this);
                } else if (this.Area.intersectionWithArea(scales.getRightPart().getCaughtArea())) {
                    scales.getRightPart().addBag(this);
                } else if (!this.Area.intersectionWithArea(ground.Area) && !selected) {
                    this.fall(ground.Area.getY());
                } else {
                    scales.getLeftPart().removeBag(this);
                    scales.getRightPart().removeBag(this);
                    fallSpeed = 0;
                }
            }
        };

        this.fall = function (upperBound) {
            fallSpeed += 1;
            var y = this.Area.getY() + this.Area.getHeight() / 2 + fallSpeed;
            this.Area.moveY(y, upperBound);
        };

        this.select = function () {
            selected = true;
        };

        this.unselect = function () {
            selected = false;
        };

        this.getId = function () {
            return id;
        };

        this.getWeight = function () {
            return weight;
        }
    };

    bagModel.prototype = Object.create(app.core.BaseModel.prototype);
    bagModel.prototype.constructor = bagModel;

    return bagModel;
})(app.core || {});