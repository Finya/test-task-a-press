app.core.BaseModel = (function () {
    var baseModel = function (x, y, width, height, source) {
        this.Area = new app.core.Area(new app.core.Point(x, y), width, height, source);
    };
    return baseModel;
})(app.core || {});