app.core.Point = (function () {
    var point = function (x, y) {
        this.x = x;
        this.y = y;
    };
    return point;
})(app.core || {});