app.core.Area = (function () {
    var  area = function (_point, _width, _height, _source) {
        var point = _point;
        var width = _width;
        var height = _height;
        var source = _source;

        this.intersectionWithPoint = function (_point) {
            return (_point.x > point.x &&
                _point.x < (point.x + width) &&
                _point.y > point.y &&
                _point.y < (point.y + height)
            );
        };

        this.intersectionWithArea = function (area) {
            return !(
                area.getX() > point.x + width ||
                area.getX() + area.getWidth() < point.x ||
                area.getY() > point.y + height ||
                area.getY() + area.getHeight() < point.y
            );
        };

        this.move = function (point, upperBound) {
            this.moveY(point.y, upperBound);
            this.moveX(point.x);
        };

        this.moveX = function (x) {
            point.x = x - width / 2;
        };

        this.moveY = function (y, upperBound) {
            if (upperBound && (upperBound < (y + height / 2))) {
                y = upperBound - height;
            } else {
                y = y - height / 2;
            }
            point.y = y;
        };

        this.incrementY = function (y) {
            point.y += y;
        };

        this.getImageSource = function () {
            return source;
        };

        this.getWidth = function () {
            return width;
        };

        this.getHeight = function () {
            return height;
        };

        this.getX = function () {
            return point.x;
        };

        this.getY = function () {
            return point.y;
        }

    };

    return area;
})(app.core || {});