app.core.ScaleArrowModel = (function () {
    var scaleArrowModel = function (x, y, width, height, source) {
        app.core.BaseModel.apply(this, arguments);
        var angle = 0;

        this.setAngle = function (newValue) {
            if (newValue > 20) {
                angle = 20;
            } else if (newValue < -20) {
                newValue = -20
            } else {
                angle = newValue;
            }
        };

        this.getAngle = function () {
            return angle;
        };
    };
    scaleArrowModel.prototype = Object.create(app.core.BaseModel.prototype);
    scaleArrowModel.prototype.constructor = scaleArrowModel;

    return scaleArrowModel;
})(app.core || {});