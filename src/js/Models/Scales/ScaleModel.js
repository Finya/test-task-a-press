app.core.ScaleModel = (function () {
    var scaleModel = function (_base, _leftPart, _rightPart, _arrow, maxWeight) {
        var leftPart = new app.core.ScaleWeightModel(
            _leftPart.x,
            _leftPart.y,
            _leftPart.width,
            _leftPart.height,
            _leftPart.source,
            _leftPart.delta,
            maxWeight
        );
        var rightPart = new app.core.ScaleWeightModel(
            _rightPart.x,
            _rightPart.y,
            _rightPart.width,
            _rightPart.height,
            _rightPart.source,
            _rightPart.delta,
            maxWeight
        );
        var base = new app.core.BaseScaleModel(
            _base.x,
            _base.y,
            _base.width,
            _base.height,
            _base.source
        );
        var arrow = new app.core.ScaleArrowModel(
            _arrow.x,
            _arrow.y,
            _arrow.width,
            _arrow.height,
            _arrow.source
        );

        this.updateScales = function () {
            var left = leftPart.getWeight();
            var right = rightPart.getWeight();
            arrow.setAngle(left - right);
        };

        this.getLeftPart = function () {
            return leftPart;
        };

        this.getRightPart = function () {
            return rightPart;
        };

        this.getBase = function () {
            return base;
        };

        this.getArrow = function () {
            return arrow;
        }
    };
    return scaleModel;
})(app.core || {});