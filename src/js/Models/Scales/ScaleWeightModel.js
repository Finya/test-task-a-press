app.core.ScaleWeightModel = (function () {
    var scaleWeightModel = function (x, y, width, height, source, delta, maxWeight) {
        app.core.BaseModel.apply(this, [x, y, width, height, source]);

        var caughtArea = new app.core.Area(
            new app.core.Point(x + width / 10, y),
            width * 0.8,
            height / 20,
            source
        );
        var bags = [];
        var maxY = this.Area.getY() + delta;
        var deltaByWeight = (maxY - this.Area.getY()) / maxWeight;

        this.addBag = function (bag) {
            if (this.scalesContainBag(bag) === false) {
                bags.push(bag);
                this.updateState(bag.getWeight());
            }
        };

        this.removeBag = function (bag) {
            var bagIndex = this.scalesContainBag(bag);
            if (bagIndex !== false) {
                var deletedBag = bags[bagIndex];
                bags.splice(bagIndex, 1);
                this.updateState(deletedBag.getWeight() * (-1));
            }
        };

        this.updateState = function (weight) {
            var delta = weight * deltaByWeight;
            if (this.Area.getY() + delta < maxY) {
                this.Area.incrementY(delta);
                caughtArea.incrementY(delta);
            }
            for (var i = 0; i < bags.length; i++) {
                bags[i].Area.incrementY(delta);
            }
        };

        this.scalesContainBag = function (bag) {
            var length = bags.length;
            for (var i = 0; i < length; i++) {
                if (bags[i].getId() === bag.getId()) {
                    return i;
                }
            }
            return false;
        };

        this.getWeight = function () {
            var weight = 0;
            for (var i = 0; i < bags.length; i++) {
                weight += bags[i].getWeight();
            }
            return weight;
        };

        this.getCaughtArea = function () {
            return caughtArea;
        }
    };

    scaleWeightModel.prototype = Object.create(app.core.BaseModel.prototype);
    scaleWeightModel.prototype.constructor = scaleWeightModel;

    return scaleWeightModel;

})(app.core.Game || {});