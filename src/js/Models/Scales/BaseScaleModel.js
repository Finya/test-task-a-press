app.core.BaseScaleModel = (function () {
    var baseScaleModel= function (x, y, width, height, source) {
        app.core.BaseModel.apply(this, arguments);
    };
    baseScaleModel.prototype = Object.create(app.core.BaseModel.prototype);
    baseScaleModel.prototype.constructor = baseScaleModel;

    return baseScaleModel;
})(app.core || {});