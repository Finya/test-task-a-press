/*
    Custom
 */
//= data.js
//= app.js
//= models/gamemodel.js
//= gamecontroller.js
//= models/base/point.js
//= models/base/area.js
//= models/base/basemodel.js
//= models/groundmodel.js
//= models/bagmodel.js
//= models/scales/basescalemodel.js
//= models/scales/scaleweightmodel.js
//= models/scales/scalemodel.js
//= models/scales/scalearrowmodel.js
//= models/gamefieldmodel.js
//= views/base/baseview.js
//= views/bagview.js
//= views/fieldview.js
//= views/gameview.js
//= views/groundview.js
//= views/scalesbaseview.js
//= views/scalesweightview.js"
//= views/scalesarrowview.js



document.addEventListener("DOMContentLoaded", ready);
function ready() {
    var field = document.getElementById("field");
    app.init(field);
}
